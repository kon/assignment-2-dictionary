import subprocess

input = ["second", "third", "cat_dog", "cat", "first-word", "dog", "bred", "catt"]
output = ["2", "3", "ᓚᘏᗢ_ᘳ≖ܫ≖ᘰ", "ᓚᘏᗢ ᓚᘏᗢ ᓚᘏᗢ ᓚᘏᗢ ᓚᘏᗢ ᓚᘏᗢ", "first word explanation", "ᘳ≖ܫ≖ᘰ", "", ""]
error = ["", "", "", "","", "","wrong key", "wrong key"]
ok = True

for i, inp, out, err in zip(range(len(input)), input, output, error):
    process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    process.stdin.write(inp.encode())
    process.stdin.close()
    st_out_res = process.stdout.read().decode()
    st_err_res = process.stderr.read().decode()

    if st_out_res != out:
        print("Test №", i + 1, "failed. Output: ", st_out_res, "Expected: ", out)
        ok = False    
    if st_err_res != err:
        ok = False   
        print("Test №", i + 1, "failed. Error: ", st_err_res, "Expected: ", err)

if ok:
    print("All tests passed")
