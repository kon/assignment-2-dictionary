ASM=nasm
ASMFLAGS=-g -f elf64
LD=ld
PYTHON=python3

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<


main.o: lib.inc dict.inc words.inc colon.inc main.asm
	$(ASM) $(ASMFLAGS) -o main.o main.asm



program: main.o dict.o lib.o
	$(LD) -o program main.o dict.o lib.o


.PHONY: test
test: program
	$(PYTHON) test.py
