%define next_element 0
%macro colon 2-*; key name
%ifid %2
    %2: 
        dq next_element; null terminated string
        db %1, 0; key value 
        %define next_element %2
%else 
    %error wrong argument %2
%endif
%endmacro