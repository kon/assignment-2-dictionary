%include "lib.inc"
%include "words.inc"
%include "dict.inc"

section .rodata
error_msg:
    db "wrong key", 0

section .bss
read_buffer:
    resb 256; 255 symbols to read word and 0 as null terminator

section .text 
global _start
_start:
    mov rdi, read_buffer
    call read_word

    mov rsi, first_word; begin of dict
    mov rdi, rax
    call find_word
    test rax, rax
    jz error
    mov rdi, rax
    call print_string
    call exit

error:
    mov rax, WRITE_SYSCALL
    mov rdi, STDERR
    mov rsi, error_msg
    mov rdx, 9
    syscall
    call exit
