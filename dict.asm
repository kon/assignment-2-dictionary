%include "lib.inc"
section .text
global find_word
;rdi rsi
find_word:
    push r12
    push r13
    mov r13, rdi
    .next:
    test rsi, rsi
    jz .not_exists
    add rsi, 8; skip quad word
    mov r12, rsi

    

    mov rdi, r13
    call string_equals

    test rax, rax
    jnz .exists

    mov rsi, [r12-8]
    jmp .next
    .exists:
    mov rax, rsi
    inc rax
    
    jmp .return
    .not_exists:
    xor rax, rax
    .return:
    pop r13
    pop r12
    ret